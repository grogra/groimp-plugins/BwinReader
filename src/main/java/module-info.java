module bwinReader {
	exports de.grogra.bwinReader;

	requires platform.swing;
	requires graph;
	requires platform;
	requires utilities;
	requires xl.core;
	requires java.desktop;
	requires java.prefs;
	requires java.xml;
	requires platform.core;
}